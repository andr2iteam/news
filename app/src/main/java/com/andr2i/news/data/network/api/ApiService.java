package com.andr2i.news.data.network.api;

import com.andr2i.news.data.models.News;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("top-headlines")
    Observable<News> getNews(
            @Query("country") String country,
            @Query("apiKey") String apiKey);

    @GET("everything")
    Observable<News> getNewsSearch(
            @Query("q") String keyword,
            @Query("language") String language,
            @Query("sortBy") String sortBy,
            @Query("apiKey") String apiKey

    );

}
