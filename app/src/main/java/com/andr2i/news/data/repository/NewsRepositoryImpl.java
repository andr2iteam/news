package com.andr2i.news.data.repository;

import com.andr2i.news.data.models.News;
import com.andr2i.news.data.network.api.ApiService;
import com.andr2i.news.domain.NewsRepository;

import javax.inject.Inject;

import io.reactivex.Observable;


public class NewsRepositoryImpl implements NewsRepository {


    private final ApiService apiInterface;


    @Inject
    public NewsRepositoryImpl(ApiService apiInterface) {
        this.apiInterface = apiInterface;
    }

    public Observable<News> getNewsSearch(String keyword, String language, String sortBy, String apiKey) {
        return apiInterface.getNewsSearch(keyword, language, sortBy, apiKey);
    }

    public Observable<News> getNews(String country, String apiKey) {
        return apiInterface.getNews(country, apiKey);
    }
}
