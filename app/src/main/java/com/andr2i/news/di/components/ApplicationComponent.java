package com.andr2i.news.di.components;


import com.andr2i.news.app.App;
import com.andr2i.news.data.network.api.ApiClient;
import com.andr2i.news.data.network.api.ApiService;
import com.andr2i.news.di.modules.ApplicationModule;
import com.andr2i.news.di.modules.BuildersModuleActivity;

import javax.inject.Singleton;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;



@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        ApplicationModule.class,
        BuildersModuleActivity.class, ApiClient.class

})
public interface ApplicationComponent extends AndroidInjector<App> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<App> {
    }

    ApiService apiService();
}