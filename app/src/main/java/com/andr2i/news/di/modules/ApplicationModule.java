package com.andr2i.news.di.modules;

import android.content.Context;
import com.andr2i.news.app.App;
import javax.inject.Singleton;
import dagger.Binds;
import dagger.Module;


@Module
public abstract class ApplicationModule {

    @Singleton
    @Binds
    abstract Context provideContext(App app);


}