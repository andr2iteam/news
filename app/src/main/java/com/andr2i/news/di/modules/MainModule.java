package com.andr2i.news.di.modules;

import com.andr2i.news.data.repository.NewsRepositoryImpl;
import com.andr2i.news.domain.InteractorMain;
import com.andr2i.news.domain.InteractorMainContract;
import com.andr2i.news.domain.NewsRepository;
import com.andr2i.news.ui.activities.presenters.MainContract;
import com.andr2i.news.ui.activities.presenters.MainPresenter;
import com.andr2i.news.ui.activities.views.MainActivity;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class MainModule {

    @Binds
    abstract MainContract.Presenter bindMainPresenter(MainPresenter presenter);

    @Binds
    abstract MainContract.View bindMainView(MainActivity view);

    @Binds
    abstract InteractorMainContract bindInteractorMainContract(InteractorMain interactor);

    @Binds
    abstract NewsRepository bindNewsRepository(NewsRepositoryImpl newsRepository);

}