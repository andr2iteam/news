package com.andr2i.news.domain;

import com.andr2i.ConstantsManager;
import com.andr2i.news.data.models.News;
import com.andr2i.news.domain.base.BaseInteractor;
import javax.inject.Inject;
import io.reactivex.Observable;

public class InteractorMain  extends BaseInteractor implements InteractorMainContract {

    private final NewsRepository mNewsRepository;

    @Inject
    public InteractorMain(NewsRepository newsRepository) {
        mNewsRepository = newsRepository;
    }

    @Override
    public Observable<News> getNewsSearch(String keyword, String language, String sortBy, String apiKey) {
        if (keyword.length() > 0 ){
         return  mNewsRepository.getNewsSearch(keyword, language, "publishedAt", ConstantsManager.API_KEY)
                 .compose(applyObservableSchedulers());
        } else {
            return mNewsRepository.getNews(ConstantsManager.COUNTRY, ConstantsManager.API_KEY)
                    .compose(applyObservableSchedulers());
        }
    }
}
