package com.andr2i.news.domain;


import com.andr2i.news.data.models.News;
import io.reactivex.Observable;



public interface InteractorMainContract {
    Observable<News> getNewsSearch(String keyword, String language, String sortBy, String apiKey);

}
