package com.andr2i.news.domain;

import com.andr2i.news.data.models.News;

import io.reactivex.Observable;

public interface NewsRepository {
     Observable<News>  getNewsSearch(String keyword, String language, String sortBy, String apiKey);
     Observable<News> getNews(String country ,String apiKey);
}
