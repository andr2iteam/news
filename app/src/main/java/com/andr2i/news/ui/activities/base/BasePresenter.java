package com.andr2i.news.ui.activities.base;

public interface BasePresenter<V> {
    void detachView();

    void init();
}
