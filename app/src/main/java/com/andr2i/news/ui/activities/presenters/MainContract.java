package com.andr2i.news.ui.activities.presenters;

import com.andr2i.news.data.models.Article;
import com.andr2i.news.ui.activities.base.BasePresenter;

import java.util.List;

public interface MainContract{

    interface View {

        void  showErrorMessage(int imageView, String title, String message);
        void showArticles(List<Article> articles);
    }

    interface Presenter extends BasePresenter<View> {

        void LoadNews(final String keyword);

    }

}
