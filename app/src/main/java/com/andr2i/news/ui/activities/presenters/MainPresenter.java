package com.andr2i.news.ui.activities.presenters;

import com.andr2i.ConstantsManager;
import com.andr2i.news.R;
import com.andr2i.news.data.models.Article;
import com.andr2i.news.data.models.News;
import com.andr2i.news.domain.InteractorMain;
import com.andr2i.news.domain.InteractorMainContract;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;


public class MainPresenter implements MainContract.Presenter {

    @Inject
    MainContract.View view;
    @Inject
    InteractorMainContract interactor;

    private List<Article> articles = new ArrayList<>();

    @Inject
    public MainPresenter() {
        //interactor = new InteractorMain();
    }


    @Override
    public void LoadNews(String keyword) {


        interactor.getNewsSearch(keyword, ConstantsManager.COUNTRY, "publishedAt", ConstantsManager.API_KEY)
                .subscribe(new Observer<News>() {

                               @Override
                               public void onSubscribe(Disposable d) {

                               }

                               @Override
                               public void onNext(News response) {
                                   articles = response.getArticle();
                                   view.showArticles(articles);
                               }

                               @Override
                               public void onError(Throwable e) {
                                   view.showErrorMessage(
                                           R.drawable.oops,
                                           "Oops..",
                                           e.toString());
                               }

                               @Override
                               public void onComplete() {

                               }
                           }
                );
    }

//    @Override
//    public void startView(MainContract.View view) {
//        this.view = view;
//    }

    @Override
    public void detachView() {
        if (view != null) view = null;
    }

    @Override
    public void init() {

    }
}
