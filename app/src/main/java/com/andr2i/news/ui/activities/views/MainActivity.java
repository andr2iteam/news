package com.andr2i.news.ui.activities.views;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.andr2i.news.ui.activities.NewsDetailActivity;
import com.andr2i.news.ui.activities.presenters.MainContract;
import com.andr2i.news.ui.adapters.Adapter;
import com.andr2i.news.R;
import com.andr2i.news.data.models.Article;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;


public class MainActivity extends DaggerAppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, MainContract.View {


    private String TAG = MainActivity.class.getSimpleName();

    private RecyclerView recyclerView;
    private Adapter adapter;

    private RecyclerView.LayoutManager layoutManager;
    private List<Article> articles = new ArrayList<>();
    private TextView topHeadline;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RelativeLayout errorLayout;
    private ImageView errorImage;
    private TextView errorTitle, errorMessage;
    private Button btnRetry;


    @Inject
    MainContract.Presenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        presenter.init();


        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);

        topHeadline = findViewById(R.id.topheadelines);


        layoutManager = new LinearLayoutManager(MainActivity.this);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);

        onLoadingSwipeRefresh("");

        errorLayout = findViewById(R.id.errorLayout);
        errorImage = findViewById(R.id.errorImage);
        errorTitle = findViewById(R.id.errorTitle);
        errorMessage = findViewById(R.id.errorMessage);
        btnRetry = findViewById(R.id.btnRetry);

    }

    private void initListener() {

        adapter.setOnItemClickListener((view, position) -> {
            ImageView imageView = view.findViewById(R.id.img);
            Intent intent = new Intent(MainActivity.this, NewsDetailActivity.class);

            Article article = articles.get(position);
            intent.putExtra(Article.class.getName(), article);


            Pair<View, String> pair = Pair.create((View) imageView, ViewCompat.getTransitionName(imageView));
            ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    MainActivity.this,
                    pair
            );


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                startActivity(intent, optionsCompat.toBundle());
            } else {
                startActivity(intent);
            }

        });
    }


    @Override
    public void onRefresh() {
        presenter.LoadNews("");
    }

    private void onLoadingSwipeRefresh(final String keyword) {

        swipeRefreshLayout.post(
                () -> presenter.LoadNews(keyword)
        );

    }

    @Override
    public void showErrorMessage(int imageView, String title, String message) {

        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
        }
        errorImage.setImageResource(imageView);
        errorTitle.setText(title);
        errorMessage.setText(message);
        btnRetry.setOnClickListener(v -> onLoadingSwipeRefresh(""));

    }

    @Override
    public void showArticles(List<Article> articles) {
        this.articles = articles;
        adapter = new Adapter(articles, MainActivity.this);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        initListener();
        topHeadline.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setRefreshing(false);

    }


}
